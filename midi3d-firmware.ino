#include <MIDIUSB.h>

// 3D MIDI Controller for HTWK/Media
// (C) 2019 by Volker Gnann
// This code is under the Apache License

#define byte char


/*****************
 *** CONSTANTS ***
 *****************/


// DIRECTION AND LED INDEXES
const byte LEFT    = 0;
const byte RIGHT   = 1;
const byte BACK    = 2;
const byte FRONT   = 3;
const byte DOWN    = 4;
const byte UP      = 5;
const byte CENTER  = 6;
const byte MIDILED = 7;
const byte DIRECTION_X = 0;
const byte DIRECTION_Y = 1;
const byte DIRECTION_Z = 2;

// INTERNAL CONSTANTS
const int NUM_BUTTONS    = 7;
const int NUM_LEDS       = 8;
const int UNCHANGED      = -1;
const int NUM_DIRECTIONS = 3;

// SYSTEM COORDINATES
const int STATEMIN = -64;
const int STATEMAX =  64;

// BUTTONS
const byte BU = 10; // UP     PCINT6
const byte BD = 11; // DOWN   PCINT7
const byte BC = 9;  // CENTER PCINT5
const byte BL = 8;  // LEFT   PCINT4
const byte BR = 1;  // RIGHT  INT3
const byte BF = 0;  // FRONT  INT2
const byte BB = 7;  // BACK   INT6
const byte buttons[NUM_BUTTONS] = {BL, BR, BF, BB, BD, BU, BC};
                    // ordered according to indexes LEFT, RIGHT, ...

// LEDS
const byte LU = 13; // UP
const byte LD = A0; // DOWN
const byte LC = 12; // CENTER
const byte LL = 4;  // LEFT
const byte LR = 5;  // RIGHT
const byte LF = 2;  // FRONT
const byte LB = 6;  // BACK
const byte LM = A4; // MIDI
const byte leds[NUM_LEDS] = {LL, LR, LF, LB, LD, LU, LC, LM};
                    // ordered according to indexes LEFT, RIGHT, ...


// CUSTOM BEHAVIOR CONSTANTS
// Tune them if needed.

const int MIDILED_TIME_MS    = 100;
const int DEBOUNCING_TIME_MS = 10;
const int INC_TIME_MS        = 50;
const int MAX_INCREMENTS[NUM_DIRECTIONS] = {30, 30, 30};
const int MIN_THRESHOLDS[NUM_DIRECTIONS] = {28, 28, 2};
const int MAX_THRESHOLDS[NUM_DIRECTIONS] = {1000, 960, 1000};
const int MIN_CENTERS[NUM_DIRECTIONS]    = {470, 470, 370};
const int MAX_CENTERS[NUM_DIRECTIONS]    = {570, 570, 470};
const byte POTIS[NUM_DIRECTIONS]         = {A3, A2, A1};
const byte MIDI_CHANNEL = 0; // Channel 1

const unsigned byte MIDI_CONTROLS[NUM_DIRECTIONS] = {10, 82, 83};



/************************
 *** GLOBAL VARIABLES ***
 ************************/



// CURRENT COORDINATE STATES
//
// currentPanpotStates[0]: -64 = left, 0 = center, +64 = right
// currentPanpotStates[1]: -64 = back, 0 = center, +64 = front
// currentPanpotStates[2]: -64 = down, 0 = center, +64 = up

int prevPanpotStates[NUM_DIRECTIONS]            = {0, 0, 0};
int currentPanpotStates[NUM_DIRECTIONS]         = {0, 0, 0};
unsigned long lastPanpotIncTime[NUM_DIRECTIONS] = {0, 0, 0};


// Button States
byte prevButtonStates[NUM_BUTTONS]    = {HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH};
byte currentButtonStates[NUM_BUTTONS] = {HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH};
byte workingButtonStates[NUM_BUTTONS] = {HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH};
unsigned long prevButtonTurnTimes[NUM_BUTTONS] = {0, 0, 0, 0, 0, 0, 0};

// LED States
byte ledStates[NUM_LEDS]  = {LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW};
unsigned long prevMidiLedTurnTime = 0;

// Current mode
bool relativeMode = false;



/****************
 ** FUNCTIONS ***
 ****************/



/**
 * Checks if a certain button has changed after debouncing.
 * This function includes the button debouncing.
 *
 * Args:
 *    buttonIndex: Direction index (LEFT, RIGHT, ..., CENTER)
 *                 indicating a button
 *
 * Returns:
 *    The current button state (HIGH/LOW) if the button has changed
 *    (i.e. has been pressed or released). If the button has not changed,
 *    UNCHANGED (-1) is returned.
 */
int checkButtonChanged(int buttonIndex) {
  int reading = digitalRead(buttons[buttonIndex]);
  if (reading != currentButtonStates[buttonIndex]) {
    currentButtonStates[buttonIndex] = reading;
    prevButtonTurnTimes[buttonIndex] = millis();
  }

  if ((reading != workingButtonStates[buttonIndex]) && (millis() >= DEBOUNCING_TIME_MS)) {
    workingButtonStates[buttonIndex] = reading;
    return reading;
  }

  return UNCHANGED;
}


/**
 * Sets a LED state and turns the respective LED on or off.
 *
 * Args:
 *    ledIndex: Direction index for LED (LEFT, RIGHT, ..., CENTER) or MIDILED
 *    newState: LOW or HIGH
 */
void setLedState(int ledIndex, int newState) {
  ledStates[ledIndex] = newState;
  digitalWrite(leds[ledIndex], newState);
}


/**
 * Checks if MIDILED_TIME_MS is over. If so, reverts the MIDI LED
 * triggering.
 */
void updateMidiLed() {
  if (millis() - prevMidiLedTurnTime > MIDILED_TIME_MS) {
    int ledState = relativeMode? HIGH : LOW;
    setLedState(MIDILED, ledState);
  }
}


/**
 *  Triggers the MIDI LED
 *  (i.e. switches the MIDI LED on if relativeMode is switched off,
 *        switches the MIDI LED off if relativeMode is switched on)
 *  The LED is untriggered in updateMidiLed(), which must be called repeatedly.
 */
void triggerMidiLed() {
  int ledState = relativeMode? LOW : HIGH;
  setLedState(MIDILED, ledState);
  prevMidiLedTurnTime = millis();
}


/**
 * Outputs the directions, states, and poti values every 500 ms.
 */
void serialDebug() {
  static unsigned long lastDebugOutput = 0;
  if (millis() - lastDebugOutput > 500) {
    lastDebugOutput = millis();
    for(int i=0; i<NUM_DIRECTIONS; ++i) {
      Serial.print("Dir. ");
      Serial.print((char)('X'+i));
      Serial.print(": State ");
      Serial.print((int)currentPanpotStates[i]);
      Serial.print(", Poti ");
      Serial.println((int)analogRead(POTIS[i]));
    }
  }
}


/**
 * Converts a coordinate state to the 0..127 range needed for MIDI.
 *
 * Args:
 *    state: Coordinate state between -64 and +64
 *
 * Returns:
 *    Byte which can be interpreted as MIDI CC value.
 */
unsigned byte coordStateToMidiRange(int state) {
  if (state < STATEMIN)
    return 0;
  else if (state >= STATEMAX)
    return 127;
  return (unsigned byte)(state - STATEMIN);
}


/**
 * Checks if one (or more) current position state has changed.
 * If so outputs the corresponding MIDI control change event
 * and triggers the MIDI LED.
 */
void updateMidi() {
  serialDebug();
  bool sent = false;

  for (int i=0; i<NUM_DIRECTIONS; ++i) {
    byte value = currentPanpotStates[i];
    if (value != prevPanpotStates[i]) {
      midiEventPacket_t cc = {0x0B, 0xB0 | MIDI_CHANNEL, MIDI_CONTROLS[i], coordStateToMidiRange(value)};
      MidiUSB.sendMIDI(cc);
      prevPanpotStates[i] = value;
      sent = true;
    }
  }

  if (sent) {
    triggerMidiLed();
    MidiUSB.flush();
  }
}


/**
 * Checks if a button is currently pressed down.
 *
 * Args:
 *    index: The direction index of the button.
 *
 * Returns:
 *    true if the button is currently pressed, false otherwise.
 */
inline bool buttonPressed(byte index) {
  return (workingButtonStates[index] == LOW);
}


/**
 * Debounces all buttons.
 * Flips the relativeMode if (and only if) both UP and DOWN buttons
 * are pressed and one of them is pressed in the current moment.
 */
void controlButtons() {
  for (int i=0; i<NUM_BUTTONS; ++i) {
    if (i==UP || i==DOWN) {
      if (checkButtonChanged(i) == LOW) {
        if (buttonPressed(UP) && buttonPressed(DOWN)) {
          relativeMode = !relativeMode;
        }
      }
    }
    else {
      checkButtonChanged(i); // and ignore result
    }
  }
}


/**
 * Updates the current panpot state in absolute mode (relativeMode==false).
 * Transforms the potentiometer value in a panpot state.
 * Treats the area around the center position (which is defined in the
 * MIN_CENTERS and MAX_CENTERS arrays for each direction) as center position.
 * Treats the areas at the edges (which are defined in the MIN_THRESHOLDS and
 * MAX_THRESHOLDS arrays) as minimum and maximum position, respectively.
 *
 * Args:
 *    direction: Direction (DIRECTION_X, DIRECTION_Y, DIRECTION_Z) to update
 *    potiValue: Value read from the potentiometer.
 */
void updateDirectionAbsolutely(byte direction, int potiValue) {
  if (potiValue < MIN_THRESHOLDS[direction]) {
    currentPanpotStates[direction] = STATEMIN;
  }
  else if (potiValue > MAX_THRESHOLDS[direction]) {
    currentPanpotStates[direction] = STATEMAX;
  }
  else if (potiValue >= MIN_CENTERS[direction] && potiValue <= MAX_CENTERS[direction]) {
    currentPanpotStates[direction] = 0;
  }
  else if (potiValue < MIN_CENTERS[direction]) {
    int minCenters = MIN_CENTERS[direction];
    long stateValue = (64 * (minCenters - potiValue));
    stateValue /= (MIN_THRESHOLDS[direction]-minCenters);
    currentPanpotStates[direction] = (int) stateValue;
  }
  else {
    int maxCenters = MAX_CENTERS[direction];
    long stateValue = (64 * (potiValue - maxCenters));
    stateValue /= (MAX_THRESHOLDS[direction]-maxCenters);
    currentPanpotStates[direction] = (int) stateValue;
  }
}



/**
 * Updates the current panpot state in relative mode (relativeMode==true).
 * Transforms the potentiometer value in an increment value that is added to the
 * panpot state every INC_TIME_MS. Treats the min/max thresholds and centers
 * like updateDirectionAbsolutely().
 *
 * Args:
 *    direction: Direction (DIRECTION_X, DIRECTION_Y, DIRECTION_Z) to update
 *    potiValue: Value read from the potentiometer.
 */
void updateDirectionRelatively(byte direction, int potiValue) {
  // Set increment
  int increment = 0;

  if (potiValue < MIN_THRESHOLDS[direction]) {
    increment = - MAX_INCREMENTS[direction];
  }
  else if (potiValue > MAX_THRESHOLDS[direction]) {
    increment = MAX_INCREMENTS[direction];
  }
  else if (potiValue < MIN_CENTERS[direction]) {
    increment = - ((MIN_CENTERS[direction] - potiValue) >> 4);
  }
  else if (potiValue > MAX_CENTERS[direction]){
    increment = (potiValue - MAX_CENTERS[direction]) >> 4;
  }

  // Update increment
  if (millis() - lastPanpotIncTime[direction] >= INC_TIME_MS) {
    lastPanpotIncTime[direction] = millis();
    int panpotState = currentPanpotStates[direction] + increment;
    if (panpotState < STATEMIN) {
      panpotState = STATEMIN;
    }
    else if (panpotState > STATEMAX) {
      panpotState = STATEMAX;
    }
    currentPanpotStates[direction] = panpotState;
  }
}


/**
 * Checks the direction buttons. Overwrites the panpot state (and hence
 * the virtual instrument direction) if they are pressed. They are pressed
 * according to the following rules:
 * - if ono of LEFT/RIGHT/UP/DOWN/FRONT/BACK buttons is pressed, the panpot
 *   state is set to the maximum of that direction.
 * - Buttons of independent directions (e.g. UP and LEFT, DOWN and FRONT, but not
 *   LEFT and RIGHT) can be pressed together and work independently.
 * - Buttons of opposite directions do not work if pressed together. The mode switch
 *   if UP and DOWN are pressed is not handled here.
 * - if one of these buttons is pressed together with the CENTER button, the sound
 *   source is positioned at the center of the regarding axis. I.e. CENTER and LEFT
 *   position the sound source at the center of the X axis. CENTER and RIGHT behave
 *   the same way.
 *
 * Args:
 *   direction: Direction to check.
 */
void updateDirectionRegardingButtons(byte direction) {
  byte extrem1 = 2*direction;
  byte extrem2 = 2*direction+1;
  if (buttonPressed(extrem1) && !buttonPressed(extrem2)) {
    currentPanpotStates[direction] = buttonPressed(CENTER)? 0 : STATEMIN;
  }
  if (buttonPressed(extrem2) && !buttonPressed(extrem1)) {
    currentPanpotStates[direction] = buttonPressed(CENTER)? 0 : STATEMAX;
  }
}


/**
 * Controls the LED indicating the directions. Each direction LED (not including
 * CENTER) is turned on iff (if and only if) the extreme position in this
 * direction is reached.
 *
 * In absolute mode, the center LED is turned on iff the center position is
 * reached in all three dimensions. In relative mode, it is turned on iff the
 * increment of the Z direction (up/down) is zero and this source is not moving.
 * This indicator is necessary because, unlike the joystick, the Z potentiometer
 * does not have a spring which returns the position to the center.
 */
 void updateDirectionLeds(byte direction) {
   int state = currentPanpotStates[direction];
   bool isInMinimumState = (state == STATEMIN);
   bool isInMaximumState = (state == STATEMAX);
   bool isInCenterState = (currentPanpotStates[DIRECTION_X] == 0)
     && (currentPanpotStates[DIRECTION_Y] == 0)
     && (currentPanpotStates[DIRECTION_Z] == 0);
   setLedState(2*direction, isInMinimumState? HIGH : LOW);
   setLedState(2*direction+1,isInMaximumState? HIGH : LOW);
   if (relativeMode) {
     int potiZ = analogRead(POTIS[DIRECTION_Z]);
     bool isPotiZCentered = (potiZ >= MIN_CENTERS[DIRECTION_Z]) && (potiZ <= MAX_CENTERS[DIRECTION_Z]);
     setLedState(CENTER, isPotiZCentered? HIGH : LOW);
   } else {
     setLedState(CENTER, isInCenterState? HIGH : LOW);
   }
 }


/**
 * Updates the panpot state according to a direction, the absolute or relative
 * mode and the buttons. Updates the according direction LEDs additionally.
 *
 * Args:
 *   direction: Direction to update
 */
void updateDirection(byte direction) {
  int potiValue = analogRead(POTIS[direction]);
  if (direction != DIRECTION_Z) { // corrects the X/Y direction value flip on the pcb
    potiValue = 1023-potiValue;
  }
  if (relativeMode) {
    updateDirectionRelatively(direction, potiValue);
  } else {
    updateDirectionAbsolutely(direction, potiValue);
  }
  updateDirectionRegardingButtons(direction);
  updateDirectionLeds(direction);
}


/**
 * Initializes the Arduino board for the MIDI controller.
 */
void setup() {
  Serial.begin(115200);

  // Set all button inputs to INPUT_PULLUP
  for (int i=0; i<NUM_BUTTONS; ++i) {
    pinMode(buttons[i], INPUT_PULLUP);
  }

  // Set all LED outputs to OUTPUT, turn LEDs on
  for (int i=0; i<NUM_LEDS; ++i) {
    pinMode(leds[i], OUTPUT);
    digitalWrite(leds[i], HIGH);
  }

  delay(1000);

  // Turn LEDs off again
  for (int i=0; i<NUM_LEDS; ++i) {
    digitalWrite(leds[i], LOW);
  }
}


/**
 * The main loop. Debounces the buttons, updates the directions and LEDs, and
 * sends the MIDI control-change messages.
 */
void loop() {
  controlButtons();
  updateDirection(DIRECTION_X);
  updateDirection(DIRECTION_Y);
  updateDirection(DIRECTION_Z);
  updateMidi();
  updateMidiLed();
}

