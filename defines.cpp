// BUTTONS
#define BU 10 // UP     PCINT6
#define BD 11 // DOWN   PCINT7
#define BC  9 // CENTER PCINT5
#define BL  8 // LEFT   PCINT4
#define BR  1 // RIGHT  INT3
#define BF  0 // FRONT  INT2
#define BB  7 // BACK   INT6

// LEDS
#define LU 13 // UP
#define LD A0 // DOWN
#define LC 12 // CENTER
#define LL 4  // LEFT
#define LR 5  // RIGHT
#define LF 2  // FRONT
#define LB 6  // BACK
#define LM A4 // MIDI

// POTS
#define PHEIG A1
#define PHORI A3
#define PVERT A2
